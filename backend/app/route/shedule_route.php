<?php 	
use App\Middleware\AuthMiddleware;

$app->group('/shedule/', function () {
    
    $this->post('add', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->shedule->add($req->getParsedBody()))
                 );
    });

    $this->put('update/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->shedule->update($req->getParsedBody(),$args['id']))
                  );
     });

     $this->get('list/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->shedule->listAll($args['id']))
                  );
     });

})->add(new AuthMiddleware($app));