<?php 	
use App\Middleware\AuthMiddleware;

$app->group('/tokenpush/', function () {
    
    $this->post('add', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->tokenpush->add($req->getParsedBody()))
                 );
    });

    $this->put('update/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->tokenpush->update($req->getParsedBody(),$args['id']))
                  );
     });

     $this->get('obtain/{id}/{plataforma}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->tokenpush->obtain($args['id'],$args['plataforma']))
                  );
     });

     $this->delete('delete/{id}/{plataforma}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->tokenpush->delete($args['id'],$args['plataforma']))
                  );
     });

})->add(new AuthMiddleware($app));