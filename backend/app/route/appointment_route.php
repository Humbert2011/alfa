<?php
use App\Middleware\AuthMiddleware;

$app->group('/appointment/', function () {
    
    $this->post('add', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->appointment->add($req->getParsedBody()))
                 );
    });

    $this->put('update/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->appointment->update($req->getParsedBody(),$args['id']))
                  );
     });

     $this->get('list/{status}/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->appointment->listAll($args['id'],$args['status']))
                  );
     });
     
     $this->get('list', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json') 
                 ->write(
                   json_encode($this->model->appointment->toList())
                );
   });

     $this->get('obtain/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->appointment->obtain($args['id']))
                  );
     });

     $this->delete('delete/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->appointment->delete($args['id']))
                  );
     });

});#->add(new AuthMiddleware($app));