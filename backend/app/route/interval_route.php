<?php 	
use App\Middleware\AuthMiddleware;

$app->group('/interval/', function () {
    
    $this->post('add', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->interval->add($req->getParsedBody()))
                 );
    });

    $this->put('update/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->interval->update($req->getParsedBody(),$args['id']))
                  );
     });

     $this->get('list', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->interval->listAll())
                  );
     });

     $this->get('obtain/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->interval->obtain($args['id']))
                  );
     });

     $this->delete('delete/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->interval->delete($args['id']))
                  );
     });

})->add(new AuthMiddleware($app));