<?php
// use App\Lib\Auth,
    // App\Lib\Response,
    // App\Validation\userValidation
    // App\Middleware\AuthMiddleware;

$app->group('/auth/', function () {
    $this->post('validateEmail', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->auth->sendCodeToEmail($req->getParsedBody()))
                  );
     });

     $this->post('validateCodeEmail',function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                        json_encode($this->model->auth->validateCodeEmail($req->getParsedBody()))
                    );
     });

     $this->post('loginAdmin',function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                        json_encode($this->model->auth->login($req->getParsedBody(),3))
                    );
     });

     $this->post('loginBusiness',function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                        json_encode($this->model->auth->login($req->getParsedBody(),2))
                    );
     });

     $this->get('getData/{token}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->getData($args['token']))
                   );
    });
    
});#->add(new Middleware($app)); #valida que en todas la validaciones en la cabecera se envie un token y va a validar dicho token