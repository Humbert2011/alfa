<?php 	
use App\Middleware\AuthMiddleware;

$app->group('/person/', function () {
    
    $this->post('add', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->person->add($req->getParsedBody()))
                 );
    });

    $this->put('update/{idPersona}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->person->add($req->getParsedBody(),$args['idPersona']))
                  );
     }); 

    $this->get('list/{tipoPersona}',function($req,$res,$args){
        return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			     json_encode($this->model->person->listAll($args['tipoPersona']))
    			 );
    });

    $this->get('list',function($req,$res,$args){
        return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			     json_encode($this->model->person->listAll())
    			 );
    });

    $this->get('obtain/{idPersona}',function($req,$res,$args){
        return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			     json_encode($this->model->person->obtain($args['idPersona']))
    			 );
    });

    $this->delete('{idPersona}',function($req,$res,$args){
        return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			     json_encode($this->model->person->delete($args['idPersona']))
    			 );
    });

})->add(new AuthMiddleware($app));