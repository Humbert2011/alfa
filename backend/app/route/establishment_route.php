<?php 	
use App\Middleware\AuthMiddleware;

$app->group('/establishment/', function () {
    
    $this->post('add', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->establishment->add($req->getParsedBody()))
                 );
    });

    $this->put('update/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->establishment->update($req->getParsedBody(),$args['id']))
                  );
     });

     $this->get('list', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->establishment->listAll())
                  );
     });

     $this->get('listForAdmin/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json') 
                 ->write(
                   json_encode($this->model->establishment->listForAdmin($args['id']))
                );
   });

     $this->get('obtain/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->establishment->obtain($args['id']))
                  );
     });

     $this->delete('delete/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->establishment->delete($args['id']))
                  );
     });

})->add(new AuthMiddleware($app));