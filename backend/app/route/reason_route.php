<?php 	
use App\Middleware\AuthMiddleware;

$app->group('/reason/', function () {
    
    $this->post('add', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->reason->add($req->getParsedBody()))
                 );
    });

    $this->put('update/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->reason->update($req->getParsedBody(),$args['id']))
                  );
     });

     $this->get('list/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->reason->listAll($args['id']))
                  );
     });

     $this->get('obtain/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->reason->obtain($args['id']))
                  );
     });

     $this->delete('delete/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json') 
                   ->write(
                     json_encode($this->model->reason->delete($args['id']))
                  );
     });

})->add(new AuthMiddleware($app));