<?php 
namespace App\Model;

use App\Lib\Response;

class ScheduleModel
{
	private $db;
	private $response;
	private $tableSchedule = 'horario';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

    public function add($data){
        $temp = count($data);
		for ($i=0; $i < $temp; $i++) { 
			$add = $this->db->insertInto($this->tableSchedule, $data[$i])
						    ->execute();
		}		
			   $this->response->result = "Horarios gurdados de manera exitosa";
		return $this->response->SetResponse(true);
    }
    
    public function listAll($id){
		$data = $this->db->from($this->tableSchedule)
							->where('negocio', $id)
							->orderBy('negocio DESC')
    					    ->fetchAll();

    		   $this->response->result = $data;
    	return $this->response->SetResponse(true);
		
	}

    public function update($data,$id){
		$eliminar = $this->db->deleteFrom($this->tableSchedule)
							 ->where('negocio',$id)
							  ->execute();
							  
		$temp = count($data);
		for ($i=0; $i < $temp; $i++) { 
			$add = $this->db->insertInto($this->tableSchedule, $data[$i])
							->execute();
		}	
			   $this->response->result = "Horarios actualizados de manera exitosa";
		return $this->response->SetResponse(true);
	}


}