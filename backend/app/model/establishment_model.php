<?php 
namespace App\Model;

use App\Lib\Response;

class EstablishmentModel 
{
	private $db;
	private $response;
	private $tableEstablishment = 'negocio';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	public function add($data){
        $register = $this->db->insertInto($this->tableEstablishment, $data)
							 ->execute(); #excute(ejecuta la consulta)

						$this->response->result = $register;
					return $this->response->SetResponse(true, "Registro exitoso");
    }

    public function update($data,$id){
        $buscar = $this->db->from($this->tableEstablishment)
	                     ->where('idnegocio', $id)
	                     ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='El Establecimiento no existe.';
	      return $this->response->SetResponse(false);
	    }else{
	    	 $actualizar = $this->db->update($this->tableEstablishment, $data) 
	                                ->where('idnegocio',$id)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
    }

    public function listAll(){
        $data = $this->db->from($this->tableEstablishment)
                         ->where("Status = 1")
                         ->fetchAll();

               $this->response->result = $data;
        return $this->response->SetResponse(true);
    }

	public function listForAdmin($idPersona){
		$data = $this->db->from($this->tableEstablishment)
						 ->where("propietario",$idPersona)
						 ->fetchAll();
			   $this->response->result = $data;
		return $this->response->SetResponse(true);
	}

    public function obtain($id){
        $data = $this->db->from($this->tableEstablishment)
						 ->where("idnegocio",$id)
						 ->fetch();
		if ($data != false) {
			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe este Establecimiento";
			return $this->response->SetResponse(false);
		}
    }

    public function delete($id){
        $data = $this->db->from($this->tableEstablishment)
						 ->where("idnegocio",$id)
						 ->fetch();
		if ($data != false) {

			$delete = $this->db->update($this->tableEstablishment)
							   ->set('Status',0)
							   ->where("idnegocio",$id)
							   ->execute();

			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe este Establecimiento";
			return $this->response->SetResponse(false);
		}
    }

}