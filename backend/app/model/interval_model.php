<?php 
namespace App\Model;

use App\Lib\Response;

class IntervalModel 
{
	private $db;
	private $response;
	private $tableInterval = 'intervalo';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

    public function add($data){
        $register = $this->db->insertInto($this->tableInterval, $data)
							 ->execute(); #excute(ejecuta la consulta)

               $this->response->result = $register;
        return $this->response->SetResponse(true, "Registro exitoso");
    }

    public function update($data,$id){
        $buscar = $this->db->from($this->tableInterval)
	                     ->where('idintervalo', $id)
	                     ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='El Intervalo no existe.';
	      return $this->response->SetResponse(false);
	    }else{
	    	 $actualizar = $this->db->update($this->tableInterval, $data) 
	                                ->where('idintervalo',$id)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
    }

    public function listAll(){
        $data = $this->db->from($this->tableInterval)
                         ->fetchAll();

               $this->response->result = $data;
        return $this->response->SetResponse(true);
    }

    public function obtain($id){
        $data = $this->db->from($this->tableInterval)
						 ->where("idintervalo",$id)
						 ->fetch();
		if ($data != false) {
			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe este Intervalo";
			return $this->response->SetResponse(false);
		}
    }

    public function delete($id){
        $data = $this->db->from($this->tableInterval)
						 ->where("idintervalo",$id)
						 ->fetch();
		if ($data != false) {

			$delete = $this->db->delete($this->tableInterval)
                               ->where("idintervalo",$id)
							   ->execute();

			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe este Intervalo";
			return $this->response->SetResponse(false);
		}
    }
}