<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\Codigos,
    App\Lib\Token;

class AuthModel{
    private $db;
    private $tbPerson = 'persona';
    private $tbTokenFirebase = 'tokenPush';
    private $tbValidateEmail = 'validarCorreo';
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function login($data,$tipoUser,$tokenFB = null){
        $user = $data['User'];
        $password = $data['Password'];
        $plataforma = $data['Plataforma'];
        $tokenFB = $data['TokenFB'];
           
        $password = Cifrado::Sha512($password);

        $usuario = $this->db->from($this->tbPerson)
                            ->select('tipoPersona.Descripcion')
                            ->where('
                                (Status <= 1) and 
                                (persona.tipoPersona = :tipoPersona and password = :Password) and
                                (Telefono = :Telefono or Correo = :Correo )',
                                array(
                                    ':tipoPersona' => $tipoUser, 
                                    ':Password' => $password, 
                                    ':Telefono' => $user, 
                                    ':Correo' => $user
                                ))
                            ->leftJoin('tipoPersona on persona.tipoPersona = tipoPersona.idtipoPersona')
                            ->fetch();

        if(is_object($usuario)){
             //actualizar activo en mysql
            //  $actualizarActivo = $this->db->update($this->tbPerson, ["activo"=>1])
            //                               ->where('idpersona', $usuario->idpersona)
            //                               ->execute();
            $mytoken = new Token();
            $token = $mytoken->addToken([
                'idpersona' => $usuario->idpersona,
                'Nombre' => $usuario->Nombre,
                'Apellidos' => $usuario->Apellidos,
                'Correo' => $usuario->Correo,
                'FotoPerfil'=>$usuario->FotoPerfil,
                'Telefono' => $usuario->Telefono,
            ]);
            // token firebase
            if($tokenFB != null){
                $data = [
                    'idpersona' => $usuario->idUsuario,
                    'token' => $tokenFB,
                    'plataforma' => $plataforma
                ];

                $idUsuario = $data['idUsuario'];
                $idToken = $this->db->from($this->tbTokenFirebase)
                                    ->select('COUNT(*) Total')
                                    ->where('idUsuario', $idUsuario)
                                    ->fetch()
                                    ->Total;

                if ($idToken > 0) {
                    $actualizatoken = $this->db->update($this->tbTokenFirebase, $data)
                                               ->where('idUsuario', $idUsuario)
                                               ->execute();
                }else{
                    $altatoken=$this->db->insertInto($this->tbTokenFirebase, $data)
                                        ->execute();
                }
            }
            // fin token firebase
            $this->response->result = [
                                'token' => $token,
                                'id' => $usuario->idpersona];
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors = "Credenciales no válidas";
            return $this->response->SetResponse(false);
        }
    }

    public function getData($data){
        $token = new Token();
        $res = $token->getData($data);
        if ($res === null) {
                   $this->response->errors = "Token incorrecto"; 
            return $this->response->SetResponse(false);
        }
               $this->response->result = $res;
        return $this->response->SetResponse(true);
    }
    #WEB
    #Enviar el código al correo
    public function sendCodeToEmail($data){ 
        $Correo  = $data['Correo'];
        $tipoPersona  = $data['tipoPersona'];

        $busca_correo_registrado = $this->db->from($this->tbPerson)
            ->select(null)
            ->select('CONCAT(Nombre, " ", Apellidos) AS NombreCompleto,idpersona')
            ->where('
                tipoPersona = :tipoPersona AND 
                Correo = :Correo AND 
                (Status <= 1)',
                array(
                    ':tipoPersona' => $tipoPersona,
                    ':Correo' => $Correo
                ))
            ->fetch();

        if ($busca_correo_registrado == false) {
            $codigo_verificacion = Codigos::Codigo(6);
            
            #Mandar al correo electrónico
            // $send_email = EmailRecoverPassword::sendEmail($email, $codigo_verificacion, $busca_correo_registrado->idUsuario, $busca_correo_registrado->NombreCompleto);

            $send_email = 1;

            if ($send_email == 1) {
                $fecha_envio = date('Y-m-d H:i:s');
                $fecha_expiracion = date('Y-m-d H:i:s', (strtotime ("+24 Hours")));

                $data_mensaje = [
                    'Codigo' => $codigo_verificacion,
                    'FechaAlta' => $fecha_envio,
                    'FechaExpiracion' => $fecha_expiracion,
                    'Status' => 1,
                    'tipoPersona' => $tipoPersona,
                    'Correo' => $Correo
                ];

                $altaCodigo = $this->db->insertInto($this->tbValidateEmail, $data_mensaje)
                                       ->execute();

                       $this->response->result = $busca_correo_registrado->NombreCompleto;
                return $this->response->SetResponse(true, 'Se ha enviado un código de verifiación al correo: '.$Correo);
            } else {
                       $this->response->errors = "No se ha podido enviar el correo electrónico.";
                return $this->response->SetResponse(false);
            }
        } else {
                   $this->response->errors = "El correo proporcionado ya está asociado a una cuenta.";
            return $this->response->SetResponse(false);
        }
    }

    #Validar el código
    public function validateCodeEmail($data){
        $Correo = $data['Correo'];
        $Codigo = $data['Codigo'];
        $tipoPersona = $data['tipoPersona'];

        $data_mensaje = $this->db->from($this->tbValidateEmail)
            ->select(null)
            ->select('idvalidarCorreo AS idRecover, Codigo')
            ->where('
                        idvalidarCorreo = (SELECT MAX(idvalidarCorreo) FROM validarCorreo 
                        WHERE Correo = :Correo AND
                        Status = 1 AND tipoPersona = :tipoPersona )',
                        array(
                            ':Correo' => $Correo,
                            ':tipoPersona' => $tipoPersona
                        )
                    )
            ->fetch();

        if ($data_mensaje != false) {
            if ($data_mensaje->Codigo === $Codigo) {
                $updateStaus = $this->db->update($this->tbValidateEmail)
                                        ->set('Status', 0)
                                        ->where('idvalidarCorreo', $data_mensaje->idRecover)
                                        ->execute();

                $token_recover_password = Token::LogReg(["tokenRegistro" => $Codigo]);

                       $this->response->result = $token_recover_password;
                return $this->response->SetResponse(true, 'Código de verificación correcto.');
            } else {
                       $this->response->errors='El código que ingresó es incorrecto.';
                return $this->response->SetResponse(false); 
            }
        } else {
                   $this->response->errors='No hay código para este email.';
            return $this->response->SetResponse(false);
        }
    }
}