<?php 
namespace App\Model;

use App\Lib\Response;

class TokenPushModel 
{
	private $db;
	private $response;
	private $tableTokenPush = 'tokenPush';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	public function add($data){
		$id = $data['idpersona'];
		$plataforma = $data['Plataforma'];

		$buscar = $this->db->from($this->tableTokenPush)
						->where('idpersona',$id)
						->where('Plataforma',$plataforma)
						->fetch();
		if($buscar){
			$register = $this->db->update($this->tableTokenPush,$data)
								 ->where('idpersona',$id)
								 ->where('Plataforma',$plataforma)
								 ->execute();
		}else{
			$register = $this->db->insertInto($this->tableTokenPush,$data)
				 			     ->execute();
		}
				$this->response->result = $data;
		return $this->response->SetResponse(true, "Registro exitoso");
    }

	public function update($data,$id){
		
		$plataforma  = $data['Plataforma'];

		$register = $this->db->update($this->tableTokenPush,$data)
								 ->where('idpersona',$id)
								 ->where('Plataforma',$plataforma)
								 ->execute();

			   $this->response->result = $data;
		return $this->response->SetResponse(true, "Registro exitoso");					 
	}

	public function obtain($id,$plataforma){
		
		$data = $this->db->from($this->tableTokenPush)
						->where('idpersona',$id)
						->where('Plataforma',$plataforma)
						->fetch();

			   $this->response->result = $data;
		return $this->response->SetResponse(true);
	}

	public function delete($id,$plataforma){
		$delete = $this->db->delete($this->tableTokenPush)
						   ->where('idpersona',$id)
						   ->where('Plataforma',$plataforma)
						   ->execute();

		       $this->response->result = $delete;
		return $this->response->SetResponse(true);
	}
}