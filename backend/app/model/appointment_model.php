<?php 
namespace App\Model;

use App\Lib\Response;

class AppointmentModel 
{
	private $db;
	private $response;
	private $tableAppointment = 'cita';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	public function add($data){
        $register = $this->db->insertInto($this->tableAppointment, $data)
							 ->execute(); #excute(ejecuta la consulta)

						$this->response->result = $register;
					return $this->response->SetResponse(true, "Registro exitoso");
    }

    public function update($data,$id){
        $buscar = $this->db->from($this->tableAppointment)
	                     ->where('idcita', $id)
	                     ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='La cita no existe.';
	      return $this->response->SetResponse(false);
	    }else{
	    	 $actualizar = $this->db->update($this->tableAppointment, $data) 
	                                ->where('idcita',$id)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
    }

    public function listAll($id = null,$status = null){
		$data = null;
        if($status == null) {
            $data = $this->db->from($this->tableAppointment)
                         ->where("negocio = :id",[":id"=>$id])
                         ->fetchAll();
        }else if ($id == null) {
			$data = $this->db->from($this->tableAppointment)
                         ->where("statusCita = :Status",[":Status"=>$status])
                         ->fetchAll();
		}{
			if ($status == 1) {
				$data = $this->db->from($this->tableAppointment)
							 ->select(null)
						 	 ->select("idcita , Fecha,Hora,Nota,  statusCita.Descripcion as statusCita, motivo.Descripcion as Motivo, usuario, NombreUsuario, ApellidosUsario, Correo, Telefono")
                         	 ->leftJoin("statusCita on cita.statusCita = statusCita.idstatusCita")
						 	 ->leftJoin(" motivo on cita.motivo = motivo.idmotivo")
                         	 ->where("statusCita <= 2 AND cita.negocio = :id",[":id"=>$id])
                          	 ->fetchAll();
			}else if($status == 0){
				$data = $this->db->from($this->tableAppointment)
							 ->select(null)
						 	 ->select("idcita , Fecha,Hora,Nota,  statusCita.Descripcion as statusCita, motivo.Descripcion as Motivo, usuario, NombreUsuario, ApellidosUsario, Correo, Telefono")
                         	 ->leftJoin("statusCita on cita.statusCita = statusCita.idstatusCita")
						 	 ->leftJoin(" motivo on cita.motivo = motivo.idmotivo")
                         	 ->where("statusCita > 2 AND cita.negocio = :id",[":id"=>$id])
                          	 ->fetchAll();
			}
		}
    
			   $this->response->result = $data;
        return $this->response->SetResponse(true);
    }
    
    public function toList(){
        $data = $this->db->from($this->tableAppointment)
						 ->select(null)
						 ->select("idcita , Fecha,Hora,Nota,  statusCita.Descripcion as statusCita, motivo.Descripcion as Motivo, usuario, NombreUsuario, ApellidosUsario, Correo, Telefono")
                         ->leftJoin("statusCita on cita.statusCita = statusCita.idstatusCita")
						 ->leftJoin(" motivo on cita.motivo = motivo.idmotivo")
						 ->fetchAll();

			   $this->response->result = $data;
		return $this->response->SetResponse(true);
    }

    public function obtain($id){
        $data = $this->db->from($this->tableAppointment)
						 ->select(null)
						 ->select("idcita , Fecha,Hora,Nota,  statusCita.Descripcion as statusCita, motivo.Descripcion as Motivo, usuario, NombreUsuario, ApellidosUsario, Correo, Telefono")
                         ->leftJoin("statusCita on cita.statusCita = statusCita.idstatusCita")
						 ->leftJoin(" motivo on cita.motivo = motivo.idmotivo")
						 ->where("idcita",$id)
						 ->fetch();
		if ($data != false) {
			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe esta Cita";
			return $this->response->SetResponse(false);
		}
    }

    public function delete($id){
        $data = $this->db->from($this->tableAppointment)
						 ->where("idcita",$id)
						 ->fetch();
		if ($data != false) {

			$delete = $this->db->update($this->tableAppointment)
							   ->set('statusCita',400)
							   ->where("idcita",$id)
							   ->execute();

			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe esta Cita";
			return $this->response->SetResponse(false);
		}
    }

}