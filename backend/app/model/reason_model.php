<?php 
namespace App\Model;

use App\Lib\Response;

class ReasonModel 
{
	private $db;
	private $response;
	private $tableReason = 'motivo';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	public function add($data){
        $register = $this->db->insertInto($this->tableReason, $data)
							 ->execute(); #excute(ejecuta la consulta)

						$this->response->result = $register;
					return $this->response->SetResponse(true, "Registro exitoso");
    }

    public function update($data,$id){
        $buscar = $this->db->from($this->tableReason)
	                     ->where('idmotivo', $id)
	                     ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='El Motivo no existe.';
	      return $this->response->SetResponse(false);
	    }else{
	    	 $actualizar = $this->db->update($this->tableReason, $data) 
	                                ->where('idmotivo',$id)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
    }

    public function listAll($id){
        $data = $this->db->from($this->tableReason)
						 ->select("Tiempo")
						 ->leftJoin("intervalo on intervalo.idintervalo = motivo.intervalo")
                         ->where("negocio",$id)
                         ->fetchAll();

               $this->response->result = $data;
        return $this->response->SetResponse(true);
    }

    public function obtain($id){
        $data = $this->db->from($this->tableReason)
						 ->select("Tiempo")
						 ->leftJoin("intervalo on intervalo.idintervalo = motivo.intervalo")
						 ->where("idmotivo",$id)
						 ->fetch();
		if ($data != false) {
			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe este Establecimiento";
			return $this->response->SetResponse(false);
		}
    }

    public function delete($id){
        $data = $this->db->from($this->tableReason)
						 ->where("idmotivo",$id)
						 ->fetch();
		if ($data != false) {

			$delete = $this->db->delete($this->tableReason)
							   ->where("idmotivo",$id)
							   ->execute();

			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe este Establecimiento";
			return $this->response->SetResponse(false);
		}
    }

}