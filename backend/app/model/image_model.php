<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Configuracion,
	App\Lib\Codigos;

class ImageModel{
	private $db;
	private $response;
	private $tablePerson = 'persona';
	private $tbGallery = 'galeria';
	private $tableEstablishmnet = 'establecimiento';
	private $tableCategoria = 'tipoestablecimiento';
	private $tableSubcategoria = 'subcategorias';
	private $tableFondoCategoria = 'fondoCategoria';
	private $tablePromociones = 'promociones_anuncios';
	private $tableProduct = 'producto';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	#Agregar foto de perfil
    public function addPhotoUser($file, $id){
    	$buscar = $this->db->from($this->tablePerson)
    					   ->where('idUsuario', $id)
    					   ->fetch();

    	if($buscar!=false){
    		//$dirSubida = 'C:/AppServ/www/back-lubogo/image/imageProfile/'; #'/var/www/html/lubo_back_dev/img/'; #local
        	//$dirServer = 'http://localhost:8080/lubo_back_dev/image/imageProfile/'; #local
            //
           $dirSubida = '/home/stardust007/public_html/lubo.com.mx/go/back-lubogo/image/'; #'/var/www/html/lubo_back_dev/img/'; #servidor
           $dirServer = 'http://lubo.com.mx/go/back-lubogo/image/'; #servidor

    	    //verificar que la imagen no tenga detalles
    		if ($file['image']['error'] > 0 or $file['image']['tmp_name']=="") {

             			$this->response->errors = 'Tenemos problemas al cargar el archivo';
     		 	return $this->response->SetResponse(false);
        	}else{
        		//contruir las rutas de acceso a las imagenes
        		$fichero_subida = $dirSubida . "-" .$id."-". basename($file['image']['name']); 
           		$NombreArchivo = $dirServer . "-"  .$id."-". basename($file['image']['name']);

            	$insertar = $this->db->update($this->tablePerson)
            					 	 ->set('urlFoto', $NombreArchivo)
            					     ->where('idUsuario', $id)
            					      ->execute();

            		if($insertar == 1){
            			move_uploaded_file($file['image']['tmp_name'], $fichero_subida);


            			       $this->response->result = $insertar;
            			return $this->response->SetResponse(true, "Se ha agregado la foto");

           		 }else{
           		 	$this->response->errors = "Error al subir";
			return $this->response->SetResponse(false);
           		 }
        	
       		 }
    	}else{
    			   $this->response->errors = "No se encontro el usuario";
			return $this->response->SetResponse(false);
    	}
	}
	
	public function add($file,$tipo,$id){
		$conf = Configuracion::getUrl();
		$tabla = "";
		$idTabla = "";
		$dirHost = $conf['host'];
		$dirServer = $conf['servidor'];

		if ($file['img']['error'] > 0 or $file['img']['tmp_name']=="") {
				   $this->response->result = $file;
				   $this->response->errors = 'Tenemos problemas al cargar el archivo';
	 		return $this->response->SetResponse(false);
		}else{
			// 
			$tringImg = $file['img']['name'];
			$arraynameImg = explode(".",$tringImg);
			$lar = count($arraynameImg) - 1;
			$tipe = $arraynameImg[$lar];
			// 
			$codigo = Codigos::Codigo(10);
			$nameImg = "$codigo"."_"."$id.$tipe";
			$dirServerForAnte = $dirServer;
			switch ($tipo) {
				case 1:
					$tabla = $this->tablePerson;
					$idTabla = "idUsuario";
					$dirHost .= "img/persona/perfil/$nameImg";
					$dirServer .= "img/persona/perfil/$nameImg";
					$dirServerForAnte .= "img/persona/perfil/";
					$campo = "urlFoto";
					break;
				case 2:
					$tabla = $this->tableEstablishmnet;
					$idTabla = "idEstablecimiento";
					$dirHost .= "img/establecimientos/perfil/$nameImg";
					$dirServer .= "img/establecimientos/perfil/$nameImg";
					$dirServerForAnte .= "img/establecimientos/perfil/";
					$campo = "urlFoto";
					break;
				case 3:
					$tabla = $this->tableEstablishmnet;
					$idTabla = "idEstablecimiento";
					$dirHost .= "img/establecimientos/portada/$nameImg";
					$dirServer .= "img/establecimientos/portada/$nameImg";
					$dirServerForAnte .= "img/establecimientos/perfil/";
					$campo = "urlPortada";
					break;
				case 4:
					$tabla = $this->tableCategoria;
					$idTabla = "idTipoEstablecimiento";
					$dirHost .= "img/categorias/$nameImg";
					$dirServer .= "img/categorias/$nameImg";
					$dirServerForAnte .= "img/categorias/perfil/";
					$campo = "urlImagen";
					break;
				case 5:
					$tabla = $this->tableSubcategoria;
					$idTabla = "idSubCategorias";
					$dirHost .= "img/subcategorias/$nameImg";
					$dirServer .= "img/subcategorias/$nameImg";
					$dirServerForAnte .= "img/subcategorias/perfil/";
					$campo = "urlImagen";
					break;
				case 6:
					$tabla = $this->tableFondoCategoria;
					$idTabla = "idTipoEstablecimiento";
					$dirHost .= "img/categorias/fondo/$nameImg";
					$dirServer .= "img/categorias/fondo/$nameImg";
					$dirServerForAnte .= "img/categorias/perfil/";
					$campo = "Url";
					break;
				case 7:
					$tabla = $this->tablePromociones;
					$idTabla = "idPromociones_Anuncios";
					$dirHost .= "img/promociones/$nameImg";
					$dirServer .= "img/promociones/$nameImg";
					$dirServerForAnte .= "img/promociones/";
					$campo = "urlImagen";
					break;
				case 8:
					$tabla = $this->tableProduct;
					$idTabla = "idProducto";
					$dirHost .= "img/productos/$nameImg";
					$dirServer .= "img/productos/$nameImg";
					$dirServerForAnte .= "img/productos/";
					$campo = "urlFoto";
					break;
			}

			try{
				$buscarImgAnterior = $this->db->from($tabla)
												->select(null)
												->select("$campo As Url")
												->where("$idTabla", $id)
												->fetch()
												->Url;
				if($buscarImgAnterior!= null){
					$urlAnte = $buscarImgAnterior;
					$arraynameImg = explode("/",$urlAnte);
					$lar = count($arraynameImg) - 1;
					$nameAnt = $arraynameImg[$lar];
					$urlBorrar = "$dirServerForAnte$nameAnt";
					unlink("$urlBorrar");
				}
				if($mover = move_uploaded_file($file['img']['tmp_name'], $dirServer)){
					$insertar = $this->db->update($tabla)
										->set("$campo", $dirHost)
										->where("$idTabla", $id)
										->execute();

					  	   $this->response->result = [
								 "tipo"=>$tabla,
								 "url"=>$dirHost,
								 "status"=>$mover
								];
					return $this->response->SetResponse(true, "Se ha agregado la foto");
				}
			}catch(\Exception $e){
					$this->response->errors='Error encontrado: '.$e->getMessage()."\n";
			return $this->response->SetResponse(false);
			}
		}
	}

	public function addEstablishmentMenuImage($file, $id_establecimiento){
		$conf = Configuracion::getUrl();
		$dir_host = $conf['host'];
		$dir_server = $conf['servidor'];

		if ($file['img']['error'] > 0 or $file['img']['tmp_name'] == "") {
				   $this->response->result = $file;
				   $this->response->errors = 'Tenemos problemas al cargar el archivo';
	  		return $this->response->SetResponse(false);
 		}else{
			#Obtener el número de imágenes que ya subió - límite 10
			$total_imagenes_menu = $this->db->from($this->tbGallery)
				->select('COUNT(*) AS Total')
				->where('idEstablecimiento', $id_establecimiento)
				->fetch()
				->Total;
			
			if ($total_imagenes_menu < 10) {
				$array_name_img = explode(".", $file['img']['name']);
				$finalCaracterTipo = count($array_name_img) - 1;
				$tipe = $array_name_img[$finalCaracterTipo];
				$codigo = Codigos::Codigo(10);
				$nombreImg = "$codigo"."_"."$id_establecimiento";
				$dir_host .= "img/galeria/$nombreImg.$tipe";
				$dir_server .= "img/galeria/$nombreImg.$tipe";
				
				try {
					if (move_uploaded_file($file['img']['tmp_name'], $dir_server)) {
						$registrar_img = $this->db->insertInto($this->tbGallery, 
							['idEstablecimiento' => $id_establecimiento, 
							'urlImagen' => $dir_host]
						)
							->execute();
						
							$this->response->result = [
								"url"=>$dir_host,
								"idGaleria"=>$registrar_img
							];
						return $this->response->SetResponse(true);
					}
				} catch (\Exception $e) {
						$this->response->errors = 'Error encontrado: '.$e->getMessage()."\n";
					return $this->response->SetResponse(false);
				}
			} else {
					   $this->response->errors = 'Has alcanzado el límite de imágenes permitidas del menú para este establecimiento';
				return $this->response->SetResponse(false);
			}
		}
	}

	public function deleteImage($idGaleria){
		$eliminarImagen = $this->db->deleteFrom($this->tbGallery)
				 ->where('idGaleria',$idGaleria)
				 ->execute();

		if	($eliminarImagen != false) {
				   $this->response->result=$eliminarImagen;
			return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
		}else{
				   $this->response->errors='Error, al eliminar la imagen';
			return $this->response->SetResponse(false);
		}
	}

	public function listImage($idEstablecimiento){
		$listarImagen = $this->db->from($this->tbGallery)
						 ->where('idEstablecimiento',$idEstablecimiento)
						 ->orderBy('idEstablecimiento DESC')
						 ->fetchAll();

		if	($listarImagen !=false)	{
				   $this->response->result=['Lista' => $listarImagen];
			return $this->response->SetResponse(true);
		}else{
				   $this->response->errors='No existe imagen perteneciente a este establecimiento';
			return $this->response->SetResponse(false);
		}
	}

	public function updateUri($tipo){
		$conf = Configuracion::getUrl();
		$tabla = "";
		$idTabla = "";
		$dirHost = $conf['host'];
			// $dirServer = $conf['servidor'];
		switch ($tipo) {
			case 1:
				$tabla = $this->tablePerson;
				$idTabla = "idUsuario";
				$campo = "urlFoto";
				$dirHost .= "img/persona/perfil/";
				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);

				break;
			case 2:
				$tabla = $this->tableEstablishmnet;
				$idTabla = "idEstablecimiento";
				$campo = "urlFoto";
				$dirHost .= "img/establecimientos/perfil/";
				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);
				
				break;
			case 3:
				$tabla = $this->tableEstablishmnet;
				$idTabla = "idEstablecimiento";
				$dirHost .= "img/establecimientos/portada/";
				$campo = "urlPortada";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);
				break;
			case 4:
				$tabla = $this->tableCategoria;
				$idTabla = "idTipoEstablecimiento";
				$dirHost .= "img/categorias/";
				$campo = "urlImagen";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);

				break;
			case 5:
				$tabla = $this->tableSubcategoria;
				$idTabla = "idSubCategorias";
				$dirHost .= "img/subcategorias/";
				$campo = "urlImagen";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);

				break;
			case 6:
				$tabla = $this->tableFondoCategoria;
				$idTabla = "idTipoEstablecimiento";
				$dirHost .= "img/categorias/fondo/";
				$campo = "Url";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);

				break;
			case 7:
				$tabla = $this->tablePromociones;
				$idTabla = "idPromociones_Anuncios";
				$dirHost .= "img/promociones/";
				$campo = "urlImagen";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);

				break;
			case 8:
				$tabla = $this->tableProduct;
				$idTabla = "idProducto";
				$dirHost .= "img/productos/";
				$campo = "urlFoto";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);

				break;
			case 9:
				$tabla = $this->tbGallery;
				$idTabla = "idGaleria";
				$dirHost .= "img/galeria/";
				$campo = "urlImagen";

				$result = $this->updateInBase($tabla,$campo,$idTabla,$dirHost);

					   $this->response->result = $result;
				return $this->response->SetResponse(true);


				break;
		}
	}

	public function updateInBase($tabla,$campo,$idTabla,$dirHost){
		
		$td = $this->db->from($tabla)
								->select(null)
								->select("$idTabla,$campo")
								->where("$campo != '' ")
								->fetchAll();
		foreach ($td as $value) {
			
			$valueAnt = $value->$campo;
			$imagenExplode = explode('/', $value->$campo);
			$nameImg = $imagenExplode[count($imagenExplode) - 1];
			// "$nameImg";
			$dir_host = "$dirHost$nameImg";
			$data = [
				$campo => $dir_host
			];
			$actualizar = $this->db->update($tabla,$data)
									->where($idTabla,$value->$idTabla)
									->execute();
			$result[] = [
				"id" => $value->$idTabla,
				"ante"=>$valueAnt,
				"host"=>$dir_host,
				"data" => $data
			  ];
		}
		return $result;
			
	}


}