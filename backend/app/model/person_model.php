<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

class PersonModel 
{
	private $db;
	private $response;
	private $tablePerson = 'persona';
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	public function add($data){
		if(filter_var($data['Correo'],FILTER_VALIDATE_EMAIL)) {
			if(strlen($data['Apellidos']) >= 2){
				#data(contiene el body que son los datos que se van a insertar en la tabla)
				$email = $this->db->from($this->tablePerson)
								->where('Correo', $data['Correo'])
								->where('tipoPersona', $data['tipoPersona'])
								->where('status != 2')
								->fetch(); #fetch cuando solo es uno solo

				if($email!=false){
						$this->response->errors = "Ya existe un usuario con esa información";
					return $this->response->SetResponse(false);
				}else{
					$data['Password'] = Cifrado::Sha512($data['Password']);

					$register = $this->db->insertInto($this->tablePerson, $data)
										->execute(); #excute(ejecuta la consulta)

						$this->response->result = $register;
					return $this->response->SetResponse(true, "Registro exitoso");
					#falta mostrar la fecha actual del registro
				}
			}else{
					   $this->response->errors='Apellido no valido.';
				return $this->response->SetResponse(false);
			}
		}else{
				   $this->response->errors='Correo no valido.';
	        return $this->response->SetResponse(false); 
		}
		$add = $this->db->insertInto($this->tablePerson, $data)
					    ->execute();
	}

	public function update($data,$idPersona){

		$buscar = $this->db->from($this->tablePerson)
	                     ->where('idpersona', $idPersona)
	                     ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='Usuario no existe.';
	      return $this->response->SetResponse(false);
	    }else{
			if (isset($data['Password'])) {
				$data['Password'] = Cifrado::Sha512($data['Password']);
			}else{
				unset($data['Password']);
			}
	    	 $actualizar = $this->db->update($this->tablePerson, $data) 
	                                ->where('idpersona',$idPersona)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
	}

	public function listAll($tipoPersona = null){
		if ($tipoPersona == null) {
			// bucar todos
			$data = $this->db->from($this->tablePerson)
							 ->where("Status = 1")
							 ->fetchAll();
		}else{
			//buscar por tipo
			 $data = $this->db->from($this->tablePerson)
			 				  ->where("Status = 1")
			 				  ->where("tipoPersona",$tipoPersona)
							  ->fetchAll();
		}
		
			   $this->response->result = $data;
		return $this->response->SetResponse(true);
	}

	public function obtain($idPersona){
		$data = $this->db->from($this->tablePerson)
						 ->where("idpersona",$idPersona)
						 ->fetch();
		if ($data != false) {
			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe esta persona";
			return $this->response->SetResponse(false);
		}
	}

	public function delete($idPersona){
		$data = $this->db->from($this->tablePerson)
						 ->where("idpersona",$idPersona)
						 ->fetch();
		if ($data != false) {

			$delete = $this->db->update($this->tablePerson)
							   ->set('Status',0)
							   ->execute();

			$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else {
			$this->response->errors = "No existe esta persona";
			return $this->response->SetResponse(false);
		}
	}
}
?>