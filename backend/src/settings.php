<?php
return [
    'settings' => [
        'displayErrorDetails' => true,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],

        // Configuración de mi APP
        'app_token_name'   => 'auth-token',
        'connectionString' => [
            'dns'  => 'mysql:host=localhost;dbname=Alfa;charset=utf8', #dev hostinguer
            'user'  => 'alfa',#dev hostinguer
            'pass' => 'AlfaBetaGaamaDelta@13'

        ]
    ],
];
