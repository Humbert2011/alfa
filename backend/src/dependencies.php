<?php

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Database
$container['db'] = function($c){
    $connectionString = $c->get('settings')['connectionString'];

    $pdo = new PDO($connectionString['dns'], $connectionString['user'], $connectionString['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

    return new FluentPDO($pdo);
};

// Models
$container['model'] = function($c){
    return (object)[
        'auth' => new App\Model\AuthModel($c->db),
        'person' => new App\Model\PersonModel($c->db),
        'establishment' => new \App\Model\EstablishmentModel($c->db),
        'interval' => new \App\Model\IntervalModel($c->db),
        'shedule' => new \App\Model\ScheduleModel($c->db),
        'reason' => new \App\Model\ReasonModel($c->db),
        'appointment' => new \App\Model\AppointmentModel($c->db),
        'tokenpush' => new \App\Model\TokenPushModel($c->db)
     ];
};
