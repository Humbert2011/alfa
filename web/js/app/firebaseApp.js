  import Api from '../api/api.js';

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyAxw6QapxPIBXhjYRWxpCxRUrMiLQ6Lmpk",
    authDomain: "alfa-ce1bf.firebaseapp.com",
    projectId: "alfa-ce1bf",
    storageBucket: "alfa-ce1bf.appspot.com",
    messagingSenderId: "266749330327",
    appId: "1:266749330327:web:a1b353058ba095aaf87ad2"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const messaging = firebase.messaging();

  messaging.requestPermission()
  .then(()=>{
    console.log('have permissions')
    //   key : BB0Bq2hTlYZpMNx4weM5pEc6ya8yQ1qmI1X7OyHh3jE03fnZOyDAj2kxeG_WuPRzHMXtZF1j44MOw7objpNidR4
    // Add the public key generated from the console here.
    messaging.getToken({ vapidKey: 'BB0Bq2hTlYZpMNx4weM5pEc6ya8yQ1qmI1X7OyHh3jE03fnZOyDAj2kxeG_WuPRzHMXtZF1j44MOw7objpNidR4' }).then(currentToken => {
      if (currentToken) {
        // Send the token to your server and update the UI if necessary
        let tokenpush = sessionStorage.getItem('tokenpush');
        if (tokenpush) {
          if (tokenpush != currentToken) {
            // actualizar
            let servicio = `tokenpush/update/${sessionStorage.getItem('idPropietario')}`;
            let parametros = {
                "Token": currentToken,
                "Plataforma" : "web"
            };
            let api = new Api(servicio,"PUT",parametros,sessionStorage.getItem('tokenEstablishmentApp'));
            let res = api.call();
            res.then(resultado => {
                if (resultado.response) {
                    sessionStorage.setItem('tokenpush',currentToken);
                    // alert('listo para obtener push')
                }else{
                  console.log(resultado)
                }
            })
          }
        } else {
          // alta
          let servicio = `tokenpush/add`;
          let parametros = {
              "idpersona": sessionStorage.getItem('idPropietario'),
              "Token": currentToken,
              "Plataforma" : "web"
          };
          let api = new Api(servicio,"POST",parametros,sessionStorage.getItem('tokenEstablishmentApp'));
          let res = api.call();
          res.then(resultado => {
              if (resultado.response) {
                  sessionStorage.setItem('tokenpush',currentToken);
                  // alert('listo para obtener push')
              }else{
                console.log(resultado)
              }
          })
        }
      } else {
        // Show permission request UI
        console.log('No registration token available. Request permission to generate one.');
        // ...
      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
      // ...
    });

  })
  .catch(err => {
      console.log(err)
  });
  
  messaging.onMessage((payload) => {
    console.log('Message received. ', payload);
    // ...
  });