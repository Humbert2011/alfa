import adminApp from './app/adminApp.js';

const formLogin = document.querySelector("#formLogin");

formLogin.addEventListener('submit',(e)=>{
    e.preventDefault();
    let user        = document.getElementById("email").value;
    let password    = document.getElementById("password").value;
    adminApp.login(user,password);
})