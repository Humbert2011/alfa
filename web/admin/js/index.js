import adminApp from './app/adminApp.js';

document.addEventListener("DOMContentLoaded", function(event) {
    adminApp.auth();    
});

const bntLogout = document.getElementById("btnLogout");

bntLogout.addEventListener('click',()=>{
    adminApp.logout();
});
