import Api from '../../../js/api/api.js';

const adminApp = {};

adminApp.quitarLoader = () =>{
    document.getElementById('loaderPage').style.display = 'none';
}

adminApp.auth = () => {
    let token = sessionStorage.getItem('tokenAdmin');
    let servicio = `auth/getData/${token}`
    let api = new Api(servicio,"GET");
    let res = api.call();
    res.then(resultado => {
        if (resultado.response === false) {
            self.location = "login.html"
        }else{
            adminApp.quitarLoader();
            let nombre = `${resultado.result.nombre} ${resultado.result.apellidos}`;
            // document.querySelector(`#nombreAdmin`).innerHTML = nombreAdmin;
        }
    });
};

adminApp.login = (user, password) => {
    let servicio = `auth/loginAdmin`;
    let parametros = {
        "User": user,
        "Password": password
    };
    let api = new Api(servicio,"POST",parametros);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            sessionStorage.setItem('tokenAdmin',resultado.result.token);
            self.location = "index.html"
        }else{
          console.log(resultado)
        }
    })

};

adminApp.logout = () => {
    sessionStorage.removeItem("tokenAdmin");
    self.location = "login.html"
}

export default adminApp;