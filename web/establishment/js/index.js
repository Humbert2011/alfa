import establishmentApp from './app/establishmentApp.js';

document.addEventListener("DOMContentLoaded", async () => {
    await establishmentApp.auth();
    await establishmentApp.listMyEstabs(sessionStorage.getItem('idPropietario'));
    // estab activo
    let idEstab = sessionStorage.getItem('idEstab');
    if (idEstab == null) {
        $('#modalSelectEstab').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#modalSelectEstab').modal('show')
    }
    // tus citas

});

const bntLogout = document.getElementById("btnLogout");
const btnSelectMyEstab = document.getElementById('btnSelectMyEstab');
const btnChangeAccount = document.getElementById('btnChangeAccount');

bntLogout.addEventListener('click',()=>{
    establishmentApp.logout();
});

btnSelectMyEstab.addEventListener('click', () => {
    let active = $('input[name="estabRadio"]:checked').val();
    if (active) {
        sessionStorage.setItem('idEstab',active);
        $('#modalSelectEstab').modal('hide');
        self.location.reload();
    }else{
        document.getElementById('alertEstabs').style.display = "block";
        setTimeout(()=>{
            document.getElementById('alertEstabs').style.display = "none";
        },2000)
    }
})

btnChangeAccount.addEventListener('click', () =>{
    document.getElementById(`estabRadio${sessionStorage.getItem("idEstab")}`).checked = true;
    document.getElementById('btnCancelSelectEstab').style.display = "block";
    document.getElementById('closeModalSelectEstab').style.display = "block";
    $('#modalSelectEstab').modal('show')
})