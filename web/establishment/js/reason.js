import establishmentApp from './app/establishmentApp.js';

document.addEventListener("DOMContentLoaded", async () => {
    await establishmentApp.auth();
    await establishmentApp.listMyEstabs(sessionStorage.getItem('idPropietario'));
    // estab activo
    let idEstab = sessionStorage.getItem('idEstab');
    if (idEstab == null) {
        $('#modalSelectEstab').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#modalSelectEstab').modal('show')
    }
    // tus motivos
    await establishmentApp.listReason();
    // intervaloss
    await establishmentApp.listInterval();
});

const bntLogout = document.getElementById("btnLogout");
const btnSelectMyEstab = document.getElementById('btnSelectMyEstab');
const btnChangeAccount = document.getElementById('btnChangeAccount');
const formMotivo = document.getElementById('formMotivo');

bntLogout.addEventListener('click',()=>{
    establishmentApp.logout();
});

btnSelectMyEstab.addEventListener('click', () => {
    let active = $('input[name="estabRadio"]:checked').val();
    if (active) {
        sessionStorage.setItem('idEstab',active);
        $('#modalSelectEstab').modal('hide');
        self.location.reload();
    }else{
        document.getElementById('alertEstabs').style.display = "block";
        setTimeout(()=>{
            document.getElementById('alertEstabs').style.display = "none";
        },2000)
    }
})

btnChangeAccount.addEventListener('click', () =>{
    document.getElementById(`estabRadio${sessionStorage.getItem("idEstab")}`).checked = true;
    document.getElementById('btnCancelSelectEstab').style.display = "block";
    document.getElementById('closeModalSelectEstab').style.display = "block";
    $('#modalSelectEstab').modal('show')
})

formMotivo.addEventListener('submit',async(e) =>{
    e.preventDefault();
    let parametros = {
        Descripcion : document.getElementById('reasonDescripcion').value,
        intervalo : document.getElementById('selectInterval').value,
        negocio : sessionStorage.getItem("idEstab")
    }
    let idReason = document.getElementById('idReason').value;
    idReason == "0" ? await establishmentApp.addReason(parametros) : await establishmentApp.updateReason(parametros,idReason);
    
} )
// al ocultar el modal de addREason
$('#modalAddMotivo').on('hidden.bs.modal', function (e) {
    document.getElementById('idReason').value = 0;
})