import establishmentApp from './app/establishmentApp.js';

document.addEventListener("DOMContentLoaded", async () => {
    await establishmentApp.auth();
    await establishmentApp.listMyEstabs(sessionStorage.getItem('idPropietario'));
    // estab activo
    let idEstab = sessionStorage.getItem('idEstab');
    if (idEstab == null) {
        $('#modalSelectEstab').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#modalSelectEstab').modal('show')
    }
    // tus horario
    await establishmentApp.listShedule();

});

const bntLogout = document.getElementById("btnLogout");
const btnSelectMyEstab = document.getElementById('btnSelectMyEstab');
const btnChangeAccount = document.getElementById('btnChangeAccount');
const tbHorarios = document.querySelector("#tbHorarios");
const btnAddShedule = document.getElementById('btnAddShedule');

bntLogout.addEventListener('click',()=>{
    establishmentApp.logout();
});

btnSelectMyEstab.addEventListener('click', () => {
    let active = $('input[name="estabRadio"]:checked').val();
    if (active) {
        sessionStorage.setItem('idEstab',active);
        $('#modalSelectEstab').modal('hide');
        self.location.reload();
    }else{
        document.getElementById('alertEstabs').style.display = "block";
        setTimeout(()=>{
            document.getElementById('alertEstabs').style.display = "none";
        },2000)
    }
})

btnChangeAccount.addEventListener('click', () =>{
    document.getElementById(`estabRadio${sessionStorage.getItem("idEstab")}`).checked = true;
    document.getElementById('btnCancelSelectEstab').style.display = "block";
    document.getElementById('closeModalSelectEstab').style.display = "block";
    $('#modalSelectEstab').modal('show')
})

// activar descativar inputs horas
tbHorarios.addEventListener("click", (e)=>{
    let check = e.toElement;
    if (check.type == "checkbox") {
        let idElemnto = check.id;
        let split = idElemnto.split("checkboxdia");
        let id = split[1];
        let checked = check.checked;
        // console.log(e);
       if (checked) {
            document.querySelector(`#timeA${id}`).disabled = false;
            document.querySelector(`#timeC${id}`).disabled = false;
       } else {
            document.querySelector(`#timeA${id}`).disabled = true;
            document.querySelector(`#timeC${id}`).disabled = true;
       }
    }
    
})

const unoOcero  = (value) =>{
    if (value) {
        return 1
    }else{
        return 0
    }
}

btnAddShedule.addEventListener("click",(e)=>{
    e.preventDefault();
    let negocio = sessionStorage.getItem('idEstab');
    let parametros = [
        {
            "negocio":negocio,
            "dia":1,
            "Apertura":document.querySelector("#timeA1").value,
            "Cierre":document.querySelector("#timeC1").value,
            "Status":unoOcero(document.querySelector("#checkboxdia1").checked)
        },
        {
            "negocio":negocio,
            "dia":2,
            "Apertura":document.querySelector("#timeA2").value,
            "Cierre":document.querySelector("#timeC2").value,
            "Status":unoOcero(document.querySelector("#checkboxdia2").checked)
        },
        {
            "negocio":negocio,
            "dia":3,
            "Apertura":document.querySelector("#timeA3").value,
            "Cierre":document.querySelector("#timeC3").value,
            "Status":unoOcero(document.querySelector("#checkboxdia3").checked)
        },
        {
            "negocio":negocio,
            "dia":4,
            "Apertura":document.querySelector("#timeA4").value,
            "Cierre":document.querySelector("#timeC4").value,
            "Status":unoOcero(document.querySelector("#checkboxdia4").checked)
        },
        {
            "negocio":negocio,
            "dia":5,
            "Apertura":document.querySelector("#timeA5").value,
            "Cierre":document.querySelector("#timeC5").value,
            "Status":unoOcero(document.querySelector("#checkboxdia5").checked)
        },
        {
            "negocio":negocio,
            "dia":6,
            "Apertura":document.querySelector("#timeA6").value,
            "Cierre":document.querySelector("#timeC6").value,
            "Status":unoOcero(document.querySelector("#checkboxdia6").checked)
        },
        {
            "negocio":negocio,
            "dia":7,
            "Apertura":document.querySelector("#timeA7").value,
            "Cierre":document.querySelector("#timeC7").value,
            "Status":unoOcero(document.querySelector("#checkboxdia7").checked)
        }
    ];
    if (document.querySelector("#existeHorario").value == 1) {
        establishmentApp.updateShedule(parametros,negocio);
    }else{
        establishmentApp.addShedule(parametros);
    }
})