import Api from '../../../js/api/api.js';

const establishmentApp = {};

establishmentApp.quitarLoader = () =>{
    document.getElementById('loaderPage').style.display = 'none';
}

establishmentApp.auth = () => {
    let token = sessionStorage.getItem('tokenEstablishmentApp');
    let servicio = `auth/getData/${token}`
    let api = new Api(servicio,"GET");
    let res = api.call();
    res.then(resultado => {
        // console.log(resultado)
        if (resultado.response === false) {
            self.location = "login.html"
        }else{
            establishmentApp.quitarLoader();
            let nombre = `${resultado.result.Nombre} ${resultado.result.Apellidos}`;
            // console.log(nombre);
            document.getElementById("nombreUser").innerText = nombre;
        }
    });
};

establishmentApp.login = (user, password) => {
    let servicio = `auth/loginBusiness`;
    let parametros = {
        "User": user,
        "Password": password
    };
    let api = new Api(servicio,"POST",parametros);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            sessionStorage.setItem('tokenEstablishmentApp',resultado.result.token);
            sessionStorage.setItem('idPropietario',resultado.result.id)
            self.location = "index.html"
        }else{
          console.log(resultado)
        }
    })

};

establishmentApp.listMyEstabs = (id) => {
    // establishment/listForAdmin/2
    let token = sessionStorage.getItem('tokenEstablishmentApp');
    let servicio = `establishment/listForAdmin/${id}`
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    res.then(resultado => {
        // console.log(resultado)
        if (resultado.response === false) {
            alert("Error al obtener informacion")
        }else{
            let data = resultado.result;
            let inner = ``;
            data.forEach(element => {
                inner +=`<li class="list-group-item d-flex justify-content-between align-items-center">
                ${element.Nombre}
                <span class="badge"><input type="radio" id ="estabRadio${element.idnegocio}" name="estabRadio" value = "${element.idnegocio}" ></span>
                </li>`; 
            });
            document.getElementById('listMyEstabs').innerHTML = inner;
        }
    });
}

establishmentApp.obtaiinInfoPersona = (id) => {
    let token = sessionStorage.getItem('tokenEstablishmentApp');
    let servicio = `person/obtain/${id}`
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    res.then(resultado => {
        // console.log(resultado)
        if (resultado.response === false) {
            alert("Error al obtener informacion")
        }else{
            const data = resultado.result;
            // console.log(data)
            const {Nombre,Apellidos,Correo,Telefono} = data; 
            document.getElementById('Nombre').value = Nombre;
            document.getElementById('Apellidos').value = Apellidos;
            document.getElementById('Correo').value = Correo;
            document.getElementById('Telefono').value = Telefono;
        }
    });
}

establishmentApp.logout = () => {
    sessionStorage.removeItem("tokenEstablishmentApp");
    sessionStorage.removeItem("idEstab");
    sessionStorage.removeItem("idPropietario");
    sessionStorage.removeItem("tokenpush")
    self.location = "login.html"
}

establishmentApp.obtainEstab = (id) => {
    // establishment/obtain/1
    let token = sessionStorage.getItem('tokenEstablishmentApp');
    let servicio = `establishment/obtain/${id}`
    let api = new Api(servicio,"GET",null,token);
    let res = api.call();
    res.then(resultado => {
        // console.log(resultado)
        if (resultado.response === false) {
            alert("Error al obtener informacion")
        }else{
            const data = resultado.result;
            const {Nombre,Direccion,Descripcion,Latitud,Longitud} = data;
            document.getElementById('NombreEstablecimiento').value = Nombre;
            document.getElementById('Direccion').value = Direccion;
            document.getElementById('Descripcion').value = Descripcion;
            document.getElementById('Latitud').value = Latitud;
            document.getElementById('Longitud').value = Longitud;
        }
    });
}

establishmentApp.updateEstab = (parametros) => {
    let id  = sessionStorage.getItem('idEstab');
    let token = sessionStorage.getItem('tokenEstablishmentApp');
    let servicio = `establishment/update/${id}`
    let api = new Api(servicio,"PUT",parametros,token);
    let res = api.call();
    res.then(resultado => {
        console.log(resultado)
        if (resultado.response === false) {
            alert("Error al actualizar")
        }else{
            let alertUpdateSucces = document.getElementById('alertUpdateSucces');
            alertUpdateSucces.style.display = "block";
            setTimeout(()=>{
                alertUpdateSucces.style.display = "none";
            },2000)
        }
    });
}

establishmentApp.listShedule = () => {
    let servicio = `shedule/list/${sessionStorage.getItem('idEstab')}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenEstablishmentApp'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result;
            let length = data.length;
            if (length!=0) {
                document.querySelector("#existeHorario").value = "1";
                for (let i = 0; i < data.length; i++) {
                    let element = data[i];
                    let check  = document.querySelector(`#checkboxdia${i+1}`);
                    let seleccionar = true;  
                    if (element.Status == 0) {
                        seleccionar = false;
                    }else{
                        let checked = check.checked;
                        document.querySelector(`#timeA${i+1}`).disabled = false;
                        document.querySelector(`#timeC${i+1}`).disabled = false;
                        
                    }
                    check.checked = seleccionar;
                    document.querySelector(`#timeA${i+1}`).value = element.Apertura
                    document.querySelector(`#timeC${i+1}`).value = element.Cierre

                }
            }
        }
    });
}
establishmentApp.addShedule = (parametros) => {
    let servicio = `shedule/add`;
    let api = new Api(servicio,"POST",parametros,sessionStorage.getItem('tokenEstablishmentApp'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            document.getElementById('alertSheduleSuccess').style.display = "block";
            setTimeout(()=>{ 
                document.getElementById('alertSheduleSuccess').style.display = "none";
             },2000);
        }else{
            document.getElementById('alertSheduleDanger').style.display = "block";
            setTimeout(()=>{ 
                document.getElementById('alertSheduleDanger').style.display = "none";
             },2000)
        }
    });
}

establishmentApp.updateShedule = (parametros,id) => {
    console.log(parametros,id)
    let servicio = `shedule/update/${id}`;
    let api = new Api(servicio,"PUT",parametros,sessionStorage.getItem('tokenEstablishmentApp'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            document.getElementById('alertSheduleSuccess').style.display = "block";
            setTimeout(()=>{ 
                document.getElementById('alertSheduleSuccess').style.display = "none";
             },2000);
        }else{
            document.getElementById('alertSheduleDanger').style.display = "block";
            setTimeout(()=>{ 
                document.getElementById('alertSheduleDanger').style.display = "none";
             },2000)
        }
    });
}

establishmentApp.initMap = (latitude, longitude) => {
    let map = null;
    // The location
    let centroMap = {lat: latitude, lng: longitude};
    // The map, centered at zone
    map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 15, 
            center: centroMap,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
    );
    
    // const icon = {
    //     url: `${refUrlPag}img/pin-completo.png`, // url
    //     scaledSize: new google.maps.Size(60,60), // size
    // };

    let marcador = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        // icon: icon,
        draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend',  (e) => {
        document.querySelector("#Latitud").value = e.latLng.lat().toFixed(6);
        document.querySelector("#Longitud").value = e.latLng.lng().toFixed(6);
        map.panTo(e.latLng);
    });

    marcador.setMap(map);
}


establishmentApp.listInterval = () => {
    let servicio = `interval/list`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenEstablishmentApp'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result;
            let inner = ``;
            data.forEach(element => {
                inner += `<option value = "${element.idintervalo}" > ${element.Tiempo} min. </option>`
            });
            document.getElementById('selectInterval').innerHTML = inner;
        }
    });
}

establishmentApp.addReason = (parametros) => {
    let servicio = `reason/add`;
    let api = new Api(servicio,"POST",parametros,sessionStorage.getItem('tokenEstablishmentApp'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            self.location.reload();
        }else{
            document.getElementById("alertAddReason").style.display = "block";
            setTimeout(()=>{
                document.getElementById('alertAddReason').style.display = "none"
            },2000)
        }
    });
}

establishmentApp.updateReason = (parametros,id) => {
    let servicio = `reason/update/${id}`;
    let api = new Api(servicio,"PUT",parametros,sessionStorage.getItem('tokenEstablishmentApp'));
    let res = api.call();
    res.then(resultado => {
        console.log(resultado)
        if (resultado.response) {
            self.location.reload();
        }else{
            document.getElementById("alertAddReason").style.display = "block";
            setTimeout(()=>{
                document.getElementById('alertAddReason').style.display = "none"
            },2000)
        }
    });
}

establishmentApp.deleteReason = (id) => {
    if(confirm("Esta seguro de eliminar este elemento???")){
        let servicio = `reason/delete/${id}`;
        let api = new Api(servicio,"DELETE",null,sessionStorage.getItem('tokenEstablishmentApp'));
        let res = api.call();
        res.then(resultado => {
            console.log(resultado)
            if (resultado.response) {
                self.location.reload();
            }else{
                document.getElementById("alertAddReason").style.display = "block";
                setTimeout(()=>{
                    document.getElementById('alertAddReason').style.display = "none"
                },2000)
            }
        });
    }
}

establishmentApp.showModalAddReason = (id,descripcion,intervalo) => {
    document.getElementById('idReason').value = id ;
    document.getElementById('reasonDescripcion').value = descripcion;
    document.getElementById('selectInterval').value  = intervalo;
    $('#modalAddMotivo').modal('show')
}

establishmentApp.listReason = () => {
    let servicio = `reason/list/${sessionStorage.getItem('idEstab')}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenEstablishmentApp'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result;
            let inner = ``;
            window.deleteReason = establishmentApp.deleteReason;
            window.showModalAddReason = establishmentApp.showModalAddReason;
            data.forEach(element => {
                inner += `<div class="card mb-12" style="max-width: 100%;">
                <div class="row g-0">
                  <div class="col-md-2">
                    <!-- <img src="..." alt="..."> -->
                    <center>
                        <br>
                        <!-- <i class="fas fa-calendar-check"></i> -->
                        <p href="#" class=" btn-primary btn-circle btn-lg">
                            <i class="fas fa-calendar-check"></i>
                        </p>
                    </center>
                  </div>
                  <div class="col-md-7">
                    <div class="card-body">
                      <h5 class="card-title">${element.Descripcion}</h5>
                      <p class="card-text">${element.Tiempo} min.</p>
                      <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                    </div>
                  </div>
                  <div class="col-md-3" >
                    <h5 class="card-title">
                        <br>
                        <a href="#" class="btn btn-info btn-circle btn-lg" onclick = "showModalAddReason(${element.idmotivo},'${element.Descripcion}',${element.intervalo})" >
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" class="btn btn-danger btn-circle btn-lg" onclick = "deleteReason(${element.idmotivo})" >
                            <i class="fas fa-trash"></i>
                        </a>
                    </h5>
                    
                </div>
                </div>
              </div>`
            });
            document.getElementById('listReason').innerHTML = inner;
        }
    });
}

export default establishmentApp;