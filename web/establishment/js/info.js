import establishmentApp from './app/establishmentApp.js';
// map
let latMapa = parseFloat(20.174633); 
let longMap = parseFloat(-98.050865);

document.addEventListener("DOMContentLoaded", async function(event) {
    await establishmentApp.auth();
    await establishmentApp.obtaiinInfoPersona(sessionStorage.getItem('idPropietario'));
    await establishmentApp.obtainEstab(sessionStorage.getItem('idEstab'));
    establishmentApp.initMap(latMapa, longMap);
    
    // estab activo
    await establishmentApp.listMyEstabs(sessionStorage.getItem('idPropietario'));
    let idEstab = sessionStorage.getItem('idEstab');
    if (idEstab == null) {
        $('#modalSelectEstab').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#modalSelectEstab').modal('show')
    }
});

const formInfoPerson = document.querySelector("#formInfoPerson");
const bntLogout = document.getElementById("btnLogout");
const btnSelectMyEstab = document.getElementById('btnSelectMyEstab');
const btnChangeAccount = document.getElementById('btnChangeAccount');
const formInfoEstablishment = document.getElementById('formInfoEstablishment');

formInfoPerson.addEventListener('submit',(e)=>{
    e.preventDefault();
})

bntLogout.addEventListener('click',()=>{
    establishmentApp.logout();
});

btnSelectMyEstab.addEventListener('click', () => {
    let active = $('input[name="estabRadio"]:checked').val();
    if (active) {
        sessionStorage.setItem('idEstab',active);
        $('#modalSelectEstab').modal('hide');
        self.location.reload();
    }else{
        document.getElementById('alertEstabs').style.display = "block";
        setTimeout(()=>{
            document.getElementById('alertEstabs').style.display = "none";
        },2000)
    }
})

btnChangeAccount.addEventListener('click', () =>{
    document.getElementById(`estabRadio${sessionStorage.getItem("idEstab")}`).checked = true;
    document.getElementById('btnCancelSelectEstab').style.display = "block";
    document.getElementById('closeModalSelectEstab').style.display = "block";
    $('#modalSelectEstab').modal('show')
})

formInfoEstablishment.addEventListener('submit', (e)=>{
    e.preventDefault();
    // falta validacion
    let parametros = {
        "Nombre":document.getElementById('NombreEstablecimiento').value,
        "Direccion" : document.getElementById('Direccion').value,
        "Descripcion" : document.getElementById('Descripcion').value,
        "Latitud": document.getElementById('Latitud').value,
        "Longitud" : document.getElementById('Longitud').value
    };

    establishmentApp.updateEstab(parametros);

})