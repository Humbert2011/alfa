-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: Alfa
-- ------------------------------------------------------
-- Server version	8.0.25-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cita`
--

DROP TABLE IF EXISTS `cita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cita` (
  `idcita` int NOT NULL AUTO_INCREMENT,
  `usuario` int DEFAULT NULL,
  `negocio` int NOT NULL,
  `statusCita` int NOT NULL DEFAULT '1',
  `Fecha` date DEFAULT NULL,
  `Hora` time DEFAULT NULL,
  `Nota` varchar(450) DEFAULT NULL,
  `motivo` int NOT NULL,
  `NombreUsuario` varchar(450) DEFAULT NULL,
  `ApellidosUsario` varchar(450) DEFAULT NULL,
  `Correo` varchar(450) DEFAULT NULL,
  `Telefono` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idcita`),
  KEY `fk_cita_persona1_idx` (`usuario`),
  KEY `fk_cita_negocio1_idx` (`negocio`),
  KEY `fk_cita_statusCita1_idx` (`statusCita`),
  KEY `fk_cita_motivo1_idx` (`motivo`),
  CONSTRAINT `fk_cita_motivo1` FOREIGN KEY (`motivo`) REFERENCES `motivo` (`idmotivo`),
  CONSTRAINT `fk_cita_negocio1` FOREIGN KEY (`negocio`) REFERENCES `negocio` (`idnegocio`),
  CONSTRAINT `fk_cita_persona1` FOREIGN KEY (`usuario`) REFERENCES `persona` (`idpersona`),
  CONSTRAINT `fk_cita_statusCita1` FOREIGN KEY (`statusCita`) REFERENCES `statusCita` (`idstatusCita`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cita`
--

LOCK TABLES `cita` WRITE;
/*!40000 ALTER TABLE `cita` DISABLE KEYS */;
INSERT INTO `cita` VALUES (1,1,1,1,'2021-05-28','16:00:00','Nececito tratar un asunto muy importante con usted',1,'Humberto','Martinez','zion_leyenda@hotmail.com','7761052213');
/*!40000 ALTER TABLE `cita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dia`
--

DROP TABLE IF EXISTS `dia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dia` (
  `iddia` int NOT NULL AUTO_INCREMENT,
  `Dia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iddia`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia`
--

LOCK TABLES `dia` WRITE;
/*!40000 ALTER TABLE `dia` DISABLE KEYS */;
INSERT INTO `dia` VALUES (1,'Monday'),(2,'Tuesday'),(3,'Wednesday'),(4,'Thursday'),(5,'Friday'),(6,'Saturday'),(7,'Sunday');
/*!40000 ALTER TABLE `dia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `horario` (
  `Apertura` time DEFAULT NULL,
  `Cierre` time DEFAULT NULL,
  `negocio` int NOT NULL,
  `dia` int NOT NULL,
  `Status` tinyint(1) DEFAULT '1',
  KEY `fk_horario_dia1_idx` (`dia`),
  KEY `fk_horario_negocio1_idx` (`negocio`),
  CONSTRAINT `fk_horario_dia1` FOREIGN KEY (`dia`) REFERENCES `dia` (`iddia`),
  CONSTRAINT `fk_horario_negocio1` FOREIGN KEY (`negocio`) REFERENCES `negocio` (`idnegocio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
INSERT INTO `horario` VALUES ('00:00:00','00:00:00',2,1,0),('00:00:00','00:00:00',2,2,0),('00:00:00','00:00:00',2,3,0),('00:00:00','00:00:00',2,4,0),('09:00:00','17:30:00',2,5,1),('10:00:00','18:00:00',2,6,1),('10:00:00','18:00:00',2,7,1),('09:00:00','18:00:00',1,1,1),('07:00:00','16:00:00',1,2,1),('07:00:00','16:00:00',1,3,1),('07:00:00','16:00:00',1,4,1),('07:00:00','16:00:00',1,5,1),('07:00:00','16:00:00',1,6,0),('07:00:00','16:00:00',1,7,0);
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intervalo`
--

DROP TABLE IF EXISTS `intervalo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `intervalo` (
  `idintervalo` int NOT NULL AUTO_INCREMENT,
  `Tiempo` int DEFAULT NULL,
  PRIMARY KEY (`idintervalo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intervalo`
--

LOCK TABLES `intervalo` WRITE;
/*!40000 ALTER TABLE `intervalo` DISABLE KEYS */;
INSERT INTO `intervalo` VALUES (1,15),(2,30),(3,60);
/*!40000 ALTER TABLE `intervalo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `motivo`
--

DROP TABLE IF EXISTS `motivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motivo` (
  `idmotivo` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) DEFAULT NULL,
  `negocio` int NOT NULL,
  `intervalo` int NOT NULL,
  PRIMARY KEY (`idmotivo`),
  KEY `fk_motivo_negocio1_idx` (`negocio`),
  KEY `fk_motivo_intervalo1_idx` (`intervalo`),
  CONSTRAINT `fk_motivo_intervalo1` FOREIGN KEY (`intervalo`) REFERENCES `intervalo` (`idintervalo`),
  CONSTRAINT `fk_motivo_negocio1` FOREIGN KEY (`negocio`) REFERENCES `negocio` (`idnegocio`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `motivo`
--

LOCK TABLES `motivo` WRITE;
/*!40000 ALTER TABLE `motivo` DISABLE KEYS */;
INSERT INTO `motivo` VALUES (1,'Cita de trabajo',1,1),(2,'Recursos Humanos',2,2),(3,'Ventas por mayoreo',2,1);
/*!40000 ALTER TABLE `motivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `negocio`
--

DROP TABLE IF EXISTS `negocio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `negocio` (
  `idnegocio` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(250) DEFAULT NULL,
  `Descripcion` varchar(450) DEFAULT NULL,
  `Logo` varchar(450) DEFAULT NULL,
  `Portada` varchar(450) DEFAULT NULL,
  `Direccion` varchar(250) DEFAULT NULL,
  `Longitud` varchar(45) DEFAULT NULL,
  `Latitud` varchar(45) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '1',
  `propietario` int DEFAULT NULL,
  PRIMARY KEY (`idnegocio`),
  KEY `fk_negocio_persona1_idx` (`propietario`),
  CONSTRAINT `fk_negocio_persona1` FOREIGN KEY (`propietario`) REFERENCES `persona` (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `negocio`
--

LOCK TABLES `negocio` WRITE;
/*!40000 ALTER TABLE `negocio` DISABLE KEYS */;
INSERT INTO `negocio` VALUES (1,'Startdust Inc S.A de C.V','Empresa de distintas ramas de tecnología',NULL,NULL,'Calle Corregidora Colonia Centro Num 45 Huauchinango Puebla','-98.052367','20.179024',1,2),(2,'Sansarita','Tacos de la mejor calidad',NULL,NULL,'Calle Corregidora Colonia Centro Num 45 Huauchinango Puebla','-98.049778','20.177802',1,2);
/*!40000 ALTER TABLE `negocio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `idpersona` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(250) DEFAULT NULL,
  `Apellidos` varchar(250) DEFAULT NULL,
  `Correo` varchar(250) DEFAULT NULL,
  `Telefono` varchar(10) DEFAULT NULL,
  `FotoPerfil` varchar(450) DEFAULT NULL,
  `Password` varchar(450) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '1',
  `tipoPersona` int NOT NULL,
  PRIMARY KEY (`idpersona`),
  KEY `fk_persona_tipoPersona_idx` (`tipoPersona`),
  CONSTRAINT `fk_persona_tipoPersona` FOREIGN KEY (`tipoPersona`) REFERENCES `tipoPersona` (`idtipoPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'Humberto','Martinez Cuautenco','zion_leyenda@hotmail.com','7761052213',NULL,'$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0',1,3),(2,'Humberto','Martinez Cuautenco','zion_leyenda@hotmail.com','7761052213',NULL,'$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0',1,2);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statusCita`
--

DROP TABLE IF EXISTS `statusCita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statusCita` (
  `idstatusCita` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idstatusCita`)
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statusCita`
--

LOCK TABLES `statusCita` WRITE;
/*!40000 ALTER TABLE `statusCita` DISABLE KEYS */;
INSERT INTO `statusCita` VALUES (1,'Pendiente'),(2,'Confirmada'),(3,'Finalizada'),(4,'Cancelada'),(404,'Not Fout');
/*!40000 ALTER TABLE `statusCita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoPersona`
--

DROP TABLE IF EXISTS `tipoPersona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipoPersona` (
  `idtipoPersona` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipoPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoPersona`
--

LOCK TABLES `tipoPersona` WRITE;
/*!40000 ALTER TABLE `tipoPersona` DISABLE KEYS */;
INSERT INTO `tipoPersona` VALUES (1,'Usuario'),(2,'Propietario Negocio'),(3,'Administrador');
/*!40000 ALTER TABLE `tipoPersona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokenPush`
--

DROP TABLE IF EXISTS `tokenPush`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tokenPush` (
  `idpersona` int NOT NULL,
  `Token` varchar(250) DEFAULT NULL,
  `Plataforma` varchar(45) DEFAULT NULL,
  KEY `fk_tokenPush_persona1_idx` (`idpersona`),
  CONSTRAINT `fk_tokenPush_persona1` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokenPush`
--

LOCK TABLES `tokenPush` WRITE;
/*!40000 ALTER TABLE `tokenPush` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokenPush` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `validarCorreo`
--

DROP TABLE IF EXISTS `validarCorreo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `validarCorreo` (
  `idvalidarCorreo` int NOT NULL AUTO_INCREMENT,
  `Correo` varchar(45) DEFAULT NULL,
  `Codigo` varchar(6) DEFAULT NULL,
  `FechaAlta` datetime DEFAULT NULL,
  `FechaExpiracion` datetime DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `tipoPersona` int NOT NULL,
  PRIMARY KEY (`idvalidarCorreo`),
  KEY `fk_validarCorreo_tipoPersona1_idx` (`tipoPersona`),
  CONSTRAINT `fk_validarCorreo_tipoPersona1` FOREIGN KEY (`tipoPersona`) REFERENCES `tipoPersona` (`idtipoPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `validarCorreo`
--

LOCK TABLES `validarCorreo` WRITE;
/*!40000 ALTER TABLE `validarCorreo` DISABLE KEYS */;
INSERT INTO `validarCorreo` VALUES (3,NULL,'PRl6Wl','2021-05-20 00:31:49','2021-05-21 00:31:49',1,3),(4,'zion_leyenda@hotmail.com','Z9c8sb','2021-05-20 00:35:05','2021-05-21 00:35:05',0,3),(5,'zion_leyenda@hotmail.com','S0tqAQ','2021-05-20 01:07:47','2021-05-21 01:07:47',0,3),(6,'zion_leyenda@hotmail.com','HBrl41','2021-05-20 18:32:35','2021-05-21 18:32:35',0,2);
/*!40000 ALTER TABLE `validarCorreo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-05 22:41:46
